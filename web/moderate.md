---
layout: page
title: "Moderate - Connect to BigBlueButton"
permalink: /moderate/
---

# Moderate LAS

If you volunteered to help out in LAS, you can gain all the moderators privileges right away, by adding here the information you received in your emails with your PIN number. 

<h2>Main Track</h2>

<form action="https://connect.linuxappsummit.org/las-main-track" method="post" enctype="multipart/form-data">
  <label for="name">Full Name:</label>
  <input type="text" id="name" name="name"><br><br>
  <label for="moderatorpin">Pin Number:</label>
  <input type="text" id="moderatorpin" name="moderatorpin"><br><br>
  <input class="button" type="submit" value="Join" style="margin-top: 0px">
</form>
