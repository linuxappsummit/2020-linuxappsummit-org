---
layout: page
title: "Call for Workshop & Break Out Session Days"
permalink: /bofs/
---

# Call for Workshop & Break Out Session Days

LAS Workshop and Break out Session days are now open! If there is a workshop or breakout session that you would like to host, please let us know by emailing us at: appsummit@lists.freedesktop.org.

You will need to provide the following:
 
 * Title of session
 * Session hosts (name, contact info)
 * Short description of the session (3-4 sentences).
 * Preferred time slot

The format of your session can be any of the following: Breakout Session, team meeting, workshop, training/teaching session.

You will find the current schedule on our wiki: [https://gitlab.com/linuxappsummit/las-2020/-/wikis/Home](https://gitlab.com/linuxappsummit/las-2020/-/wikis/Home).

If you have any questions, feel free to reach out to us at: appsummit@lists.freedesktop.org.
