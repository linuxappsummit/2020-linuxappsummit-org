---
layout: page
title: "Schedule"
permalink: /schedule/
---

# Conference Schedule

Here you can find the schedule with all the presentations, break out sessions and social events we will be doing during LAS.

 * [Thursday 12th November 2020](https://conf.linuxappsummit.org/event/1/timetable/#20201112)
 * [Friday 13th November 2020](https://conf.linuxappsummit.org/event/1/timetable/#20201113)
 * [Saturday 14th November 2020](https://conf.linuxappsummit.org/event/1/timetable/#20201114)

## Workshop & BoF Days

* [Workshop & BoF Days!](https://linuxappsummit.org/bofs/)

