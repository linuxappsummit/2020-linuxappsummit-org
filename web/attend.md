---
layout: page
title: "Attend - Connect to BigBlueButton"
permalink: /attend/
---

# Attend LAS

This online edition of LinuxAppSummit is taking place on our instance of BigBlueButton.

If you would like to connect with the Main Track Room, where all talks will be held and you can chat with others and ask questions to the speakers, please use the form below to connect.

<h2>Main Track</h2>

<form action="https://connect.linuxappsummit.org/las-main-track" method="post" enctype="multipart/form-data">
  <label for="name">Full Name:</label>
  <input type="text" id="name" name="name"><br><br>
  <label for="attendeepin">Pin Number:</label>
  <input type="text" id="attendeepin" name="attendeepin"><br><br>
  <input class="button" type="submit" value="Join" style="margin-top: 0px">
</form>

# Socialize in LAS

The Hallway track is one of the most enjoyable parts of the conference. 

Join the room and talk with each other with the same pin number you used to join the main track.

<h2>Hallway Track</h2>

<form action="https://connect.linuxappsummit.org/las-hallway-track" method="post" enctype="multipart/form-data">
  <label for="name">Full Name:</label>
  <input type="text" id="name" name="name"><br><br>
  <label for="attendeepin">Pin Number:</label>
  <input type="text" id="attendeepin" name="attendeepin"><br><br>
  <input class="button" type="submit" value="Join" style="margin-top: 0px">
</form>