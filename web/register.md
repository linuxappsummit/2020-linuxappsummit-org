---
layout: page
title: "Register"
permalink: /register/
---
# Registration is now open!

The Linux App Summit attendance is free of charge, but you must register to attend the event.

<button class="button" onclick="window.location='https://conf.linuxappsummit.org/event/1/registrations/2/'">Register Here!</button> <button class="button" onclick="window.location.href='https://conf.linuxappsummit.org/event/1/timetable/'">Program</button>


We also encourage attendees to join the <a href="https://t.me/joinchat/Dbb0PlJjx4tPFCSu2dd7PA">LAS 2020 Telegram channel</a> and follow us on <a href="https://twitter.com/LinuxAppSummit">Twitter @linuxappsummit</a> for the most up-to-date news.
