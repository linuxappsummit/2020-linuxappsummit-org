---
layout: page
title: "Sponsors"
permalink: /sponsors/
---

# LAS Sponsors

Would you like to help make the Linux App Summit possible? Join us and [sponsor](https://linuxappsummit.org/sponsor/)!

## CHAMPION

### Canonical

<img class="sponsorlogo" src="/assets/canonical.png" alt="Canonical"/>

> Canonical is the publisher of Ubuntu, the OS for most public cloud workloads as well as the emerging categories of smart gateways, self-driving cars and advanced robots. Canonical provides enterprise security, support and services to commercial users of Ubuntu. Established in 2004, Canonical is a privately held company.
>
> [https://canonical.com](https://canonical.com)

### Collabora

<img class="sponsorlogo" src="/assets/collabora.svg" alt="Collabora"/>

> Collabora is a leading global consultancy specializing in delivering the benefits of Open Source software to the commercial world. For over 15 years, we've helped clients navigate the ever-evolving world of Open Source, enabling them to develop the best solutions – whether writing a line of code or shaping a longer-term strategic software development plan. By harnessing the potential of community-driven Open Source projects, and re-using existing components, we help our clients reduce time to market and focus on creating product differentiation.
>
> [https://collabora.com](https://collabora.com) <a href="https://twitter.com/Collabora" title="@Collabora"><svg class="svg-icon"><use xlink:href="{{ '/assets/minima-social-icons.svg#twitter' | relative_url }}"></use></svg></a>


## SUPPORTER

### Codethink

<img class="sponsorlogo" src="/assets/codethink.svg" alt="Codethink"/>

> Codethink is an ethical, independent and versatile software services company, based in Manchester, UK. We are expert in the use of Open Source technologies for systems software engineering.
>
> Leveraging Open Source tools and techniques, we help customers to meet demanding challenges with unique innovations and accelerated productivity.
>
> We can analyse, develop, integrate, optimise and support bespoke solutions for your systems in IoT, automotive, finance and anything in between.
>
> [https://www.codethink.co.uk](https://www.codethink.co.uk)

### elementary

<img class="sponsorlogo" src="/assets/elementary-wide.svg" alt="elementary" />

> elementary, Inc. leverages the unique combination of top-notch UX and the world-changing power of Open Source to deliver elementary OS—the fast, open, and privacy-respecting replacement for Windows and macOS—plus AppCenter, the pay-what-you-want app store.
>
> [https://elementary.io](https://elementary.io)

### Red Hat

<img class="sponsorlogo" src="/assets/redhat.png" alt="Red Hat"/>

> Red Hat is an enterprise software company with an open source
> development model. With engineers connected to open source communities,
> the freedom of our subscription model, and a broad portfolio of products
> that's constantly expanding, Red Hat is here to help you face your
> business challenges head-on.
>
> [https://redhat.com](https://redhat.com)

### openSUSE

<img class="sponsorlogo" src="/assets/opensuse.png" alt="openSUSE"/>

> The openSUSE project is a worldwide effort that promotes the use of Linux everywhere. The openSUSE community develops and maintains a packaging and distribution infrastructure, which provides the foundation for the world's most flexible and powerful Linux distribution.
>
> Our community works together in an open, transparent and friendly manner as part of the global Free and Open Source Software community.
>
> [https://opensuse.org](https://opensuse.org)
